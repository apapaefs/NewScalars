from matplotlib import pyplot as plt # plotting
import matplotlib.gridspec as gridspec # more plotting 
import pylab as pl

###########################
print('---')
print('plotting')

# plot settings ########
plot_type = 'mgammagamma' # the name of the plot
# the following labels are in LaTeX, but instead of a single slash, two "\\" are required.
ylab = '$\\frac{1}{\\sigma} \\frac{\\mathrm{d} \\sigma}{\\mathrm{d} m_{\\gamma\\gamma}} $' # the ylabel
xlab = '$m_{\\gamma\gamma}$ [GeV]' # the x label
# log scale?
ylog = False # whether to plot y in log scale
xlog = False # whether to plot x in log scale

# construct the axes for the plot
# no need to modify this if you just need one plot
gs = gridspec.GridSpec(4, 4)
fig = plt.figure()
ax = fig.add_subplot(111)
ax.grid(False)

# read in the file in X, Y format:
datalist = [ ( pl.loadtxt('mgammagamma.dat'), 'mass' ) ]
for data, label in datalist:
    X = data[:,0]
    Y = data[:,1]

# plot just points, no line
# set lw to a number different to zero if you want a line as well
plt.plot(X,Y, color='red', lw=0, marker='x', ms=3, label='no error')

# the following plots with error bars. Here we assume that the error is simply 0.05 * Y.
# we also shift Y slightly to tell the two examples apart
plt.errorbar(X, 0.8*Y, yerr=0.05*Y, color='blue', lw=0, fmt='o', capthick=1, capsize=1, elinewidth=1, ms=2, label='some error')


# set the ticks, labels and limits etc.
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)

# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

# set the limits on the x and y axes if required below:
#xmin = 0.
#xmax = 1500.
#ymin = 0.
#ymax = 0.09
#pl.xlim([xmin,xmax])
#pl.ylim([ymin,ymax])

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', infile.replace('.dat','.pdf'))
plt.savefig(infile.replace('.dat','.pdf'), bbox_inches='tight')
plt.close(fig)
